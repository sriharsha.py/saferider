package com.example.currentplacedetailsonmap;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.VideoView;

public class LauncherActivity extends AppCompatActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
////        //Present user UI which uses data loaded in the background.
////        startActivity(new Intent(this, MainActivity.class));
//
//
//        Handler h = new Handler();
//// The Runnable will be executed after the given delay time
//        h.postDelayed(r, 4*1000); // will be delayed for 1.5 seconds
//
//
//
//    }
//    Runnable r = new Runnable() {
//        @Override
//        public void run() {
//            // if you are redirecting from a fragment then use getActivity() as the context.
//                   startActivity(new Intent(LauncherActivity.this, MainActivity.class));
//
//            // To close the CurrentActitity, r.g. SpalshActivity
//            finish();
//        }
//    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            VideoView videoHolder = new VideoView(this);
            setContentView(videoHolder);
//            DisplayMetrics metrics = new DisplayMetrics(); getWindowManager().getDefaultDisplay().getMetrics(metrics);
//            android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoHolder.getLayoutParams();
//            params.width =  metrics.widthPixels;
//            params.height = metrics.heightPixels;
//            params.leftMargin = 0;

            //videoHolder.setLayoutParams(params);
            Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash);
            videoHolder.setVideoURI(video);

            videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    jump();
                }
            });
            videoHolder.start();
        } catch (Exception ex) {
            jump();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        jump();
        return true;
    }

    private void jump() {
        if (isFinishing())
            return;
        startActivity(new Intent(this, MapsActivityCurrentPlace.class));
        finish();
    }

}
