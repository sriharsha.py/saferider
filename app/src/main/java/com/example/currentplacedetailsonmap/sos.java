package com.example.currentplacedetailsonmap;

import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;

import java.util.Locale;

import static android.R.attr.button;

public class sos extends AppCompatActivity {
    //ui
    private Button safe;
    private Button notSafe;
    private TextView time;
    private CountDownTimer countDownTimer;
    // details of the phone call fetch from firebase
    private String phoneNo;
    private String message;
    private String gpsLocation;
    TextToSpeech t1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos);
        safe=(Button)findViewById(R.id.btn_safe);
        notSafe=(Button)findViewById(R.id.btn_notSafe);
        time=(TextView)findViewById(R.id.tv_time);
        time.setShadowLayer(10, 10, 10, Color.GRAY);

        safe.setOnClickListener(btnClickListener);
        notSafe.setOnClickListener(btnClickListener);


        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });


        init();
    }
    private void init() {
        //get  data from firebase

        meta obj = new meta();
        String lat = Double.toString(obj.lat);
        String lng = Double.toString(obj.lng);
        gpsLocation=lat+","+lng;
        phoneNo="9445430693";
        message="EMERGENCY ! Please Send Help! Location - "+ gpsLocation;
        start();
        // wait for timer or button to be pressed
    }
    //onclick to chose what to do
    private View.OnClickListener btnClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.btn_safe:
                    Intent i = new Intent(sos.this,MapsActivityCurrentPlace.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                    break;
                case R.id.btn_notSafe:
                    //immediatly trigger sos signal
                    sendSMS(phoneNo,message);
                    break;
            }
        }
    };
    private void start() {

        //start the countdownTimer
        time.setText("60");

        countDownTimer : new CountDownTimer(60*1000, 1000) {

            public void onTick(long millisUntilFinished) {
                if ((millisUntilFinished/1000) %10 == 0){
                    t1.speak("are you okay", TextToSpeech.QUEUE_FLUSH, null);
                }
                time.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                //time.setTextSize(18);
                sendSMS(phoneNo,message);
                time.setText("Help is on the way!");
                t1.speak("Help is on the way!", TextToSpeech.QUEUE_FLUSH, null);

            }
        }.start();
    }

    private void cancel() {
        // stop the countdown timer and go back to the activity
        if(countDownTimer != null) {
            countDownTimer = null;
            countDownTimer.cancel();
        }

    }
    //sos signal triggering
    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "SOS signal sent !",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(),ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    public void onPause(){
        if(t1 !=null){
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }
}
